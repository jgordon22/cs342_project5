

import java.awt.Color;


public class P10Card {
	private char rank;
	private char color;
	private String textColor;
	private static char[] ranks = {'1','2','3','4','5','6','7','8','9','T','E','L','S','W'};
	//T - Ten; E - Eleven; T - Twelve; S - Skip card; W - Wild card
    private static char[] colors = {'R','O','B','G'}; //Red, Orange, Blue, Green
    private static String[] cardColors = {"\uFF0000", "\uFFA500", "0000FF", "008000"}; //set to Red, Orange, Blue, Green

	//link to java colors - http://www.nbdtech.com/images/blog/20080427/AllColors.png

	public P10Card(int r , int c){
		rank = ranks[r];
		color = colors[c];
		textColor = cardColors[c];
	}
	
	public P10Card(char r, char c){
		rank = r;
		color = c;
		for(int i = 0; i<cardColors.length; i++){
			if(c == colors[i]){
				textColor = cardColors[i];
				break;
			}
		}
	}
	
    public int rankNo(){
        if(this.rank == '2')
            return 2;
        if(this.rank =='3')
            return 3;
        if(this.rank =='4')
            return 4;
        if(this.rank =='5')
            return 5;
        if(this.rank == '6')
            return 6;
        if(this.rank == '7')
            return 7;
        if(this.rank == '8')
            return 8;
        if(this.rank == '9')
            return 9;
        if(this.rank == 'T')
            return 10;
        if(this.rank == 'E')
            return 11;
        if(this.rank == 'L')
            return 12;
        if(this.rank == 'W')
            return 69;
        else
            return -1;
    }

	
    public String getString(){
        String result = "" + this.textColor;
        if(this.rank == 'T')
            result+="10";
        else if(this.rank == 'E')
            result+="11";
        else if(this.rank == 'L')
            result+="12";
        else
            result+=this.rank;
        result+="\u000000";
        return result;
    }


	public char getRank(){
		return this.rank;
	}

	public char getColor(){
		return this.color;
	}

	public String getTextColor(){
		return this.textColor;
	}

	public static char[] getRanks(){
		return ranks;
	}

	public static char[] getColors(){
		return colors;
	}

}
