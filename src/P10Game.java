

import java.util.ArrayList;
import java.util.Collections;


public class P10Game extends Thread{
	ArrayList<P10Card> deck;
	ArrayList<P10Card> discardPile;
	ArrayList<ClientUser> players;
	private static int numPhases = 10;
	private static boolean gameOver;
	private static boolean waitForTurn,roundOver;
	private static ArrayList<ClientUser> winners;
	int startingPlayer;
	StallThread staller;

	public P10Game(ArrayList<ClientUser> people){
		winners = new ArrayList<ClientUser>();
		startingPlayer = 0;
		gameOver = false;
		waitForTurn=true;
		roundOver = false;
		players = people;
		deck = new ArrayList<P10Card>();
		discardPile = new ArrayList<P10Card>();
		initDeck();
		clearDiscard(); //**
		shuffle();//**
		//** = Done at the end of each round
	}

	public void run(){
		while(!gameOver){
			initGame();
			playRound();
			for(int i =0; i < players.size(); i++){
				if(players.get(i).getP10Phase()>10){
					gameOver = true;
					winners.add(players.get(i));
				}
			}
		}
		String winnerName = "";
		int minScore = Integer.MAX_VALUE;
		for(int i =0; i<winners.size(); i++)
			if(winners.get(i).getP10Score()<minScore){
				minScore = winners.get(i).getP10Score();
				winnerName = winners.get(i).getName();
			}
		for(int j =0; j<players.size(); j++)//notifying users whose turn it currently is
			players.get(j).out.println(winnerName + " has completed Phase 10 and won the game!");

	}

	private void playRound(){
		int i = 0;
		while(!roundOver){
			for(i = 0;i <players.size(); i++){//looping through players for each turn
				
				if(!players.get(i).isSkipped()&&players.get(i).getActive()){
					for(int j =0; j<players.size(); j++)//notifying users whose turn it currently is
						players.get(j).out.println("It is now " + players.get(i).getName() + "'s turn.");
                    staller = new StallThread();
                    staller.start();
                    while(!this.staller.isInterrupted()){}
					if(players.get(i).getP10Hand().size() == 0){
						roundOver = true;
						break;
					}
				}
				else{
					for(int j =0; j<players.size(); j++)//notifying users whose turn it currently is
						players.get(j).out.println(players.get(i).getName() + "'s turn has been skipped.");
					players.get(i).turnSkipped();
				}
			}
		}
		endRound(i);
	}
	
	

	public void dealCards(){
		for(int i = 0; i<10; i++)//deal 10 cards to each player round-robin
			for(int j= 0; j<this.players.size(); j++)
				drawFromDeck(players.get(j));
		for(int i = 0; i < players.size(); i++)
			players.get(i).getOut().println("Cards are being dealt to each player...done.");
	}

	public void turnOver(){
		staller.interrupt();
	}

	public void initGame(){
		shuffle();
		dealCards();
		int topIndex = deck.size()-1;//take the top card from the deck and place it in the discard pile
		P10Card topCard = deck.get(topIndex);
		deck.remove(topIndex);
		discardPile.add(topCard);
		for(int j =0; j<players.size(); j++)//notifying users whose turn it currently is
			players.get(j).out.println("DISCARD " + topCard.getRank() + " " + topCard.getColor());
	}

	public void initDeck(){
		//initialize 1-12 with all 4 colors twice to get 96 cards
		for(int k = 0; k<2 ; k++)
			for(int i = 0; i < 12 ; i++)
				for(int j = 0; j<4 ; j++)
					deck.add(new P10Card(i,j));
		//96 + 4 Skip cards + 8 Wild cards = 108 total cards
		for(int i = 0; i<4; i++)
			deck.add(new P10Card(12,1));//Skip cards
		for(int i = 0; i<2; i++)
			for(int j = 0; j<4; j++)
				deck.add(new P10Card(13,j));//Wild cards
	}
    public String drawFromDiscard(ClientUser player){
        int topIndex = discardPile.size()-1;
        P10Card topCard = discardPile.remove(topIndex);
        P10Card newTop = discardPile.get(discardPile.size()-1);
        player.getOut().println("CARD "+player.getName()+" " + topCard.getRank() + " " + topCard.getColor());
        String card = Character.toString(newTop.getRank()) + Character.toString(newTop.getColor());
        return card;	
		//player.getOut().println("CARD " + topCard.getRank() + " " + topCard.getColor());
	}
    public void discardCard(String card){
        P10Card c = new P10Card(card.charAt(0), card.charAt(1));
        discardPile.add(c);
    }
	
	public void drawFromDeck(ClientUser player){
		int topIndex = deck.size()-1;
		P10Card topCard = deck.get(topIndex);
		deck.remove(topIndex);
		player.p10DrawCard(topCard);
		player.getOut().println("CARD " +player.getName()+" "+topCard.getRank() + " " + topCard.getColor());
		System.out.println(topCard.toString());
	}

	public void clearDiscard(){//add discardPile back to the deck and clear it afterwards
		deck.addAll(discardPile);
		discardPile.clear();
	}

	public void endRound(int winnerIdx){//tally points, clear the discard, and check for phase completion
		for(int i = 0; i<players.size();i++){
			if(i != winnerIdx)
				players.get(i).p10TallyPoints();
			if(players.get(i).isPhaseComplete())
				players.get(i).p10CompletePhase();
			this.deck.addAll(players.get(i).clearHand());//return players' hands to the deck
		}
		String endRndMsg = "";
		for(int i = 0; i<players.size(); i++)
			endRndMsg += players.get(i).getName() + "\n  Phase: " + players.get(i).getP10Phase() + "\n  Score: " + players.get(i).getP10Score();
		for(int i = 0; i < players.size(); i++)
			players.get(i).getOut().println("The round is now over! Post-round results:\n" + endRndMsg);
		this.clearDiscard();//clear discard pile and return it to the deck
		this.shuffle();
		this.startingPlayer++;
		if(this.startingPlayer >= players.size())
			startingPlayer = 0;
		System.out.println();
	}

	public void shuffle(){
		/*for(int i=0;i<1000;i++){
			int rand1=(int)Math.floor(Math.random()*52);
			int rand2=(int)Math.floor(Math.random()*52);
			P10Card temp=this.deck.get(rand1);
			this.deck.set(rand1,this.deck.get(rand2));
			this.deck.set(rand2,temp);
		}*/
		Collections.shuffle(this.deck);
		for(int i = 0; i < players.size(); i++)
			players.get(i).getOut().println("The deck is being shuffled...done.");
	}



}
