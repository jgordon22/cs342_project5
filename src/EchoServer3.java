
import java.net.*; 
import java.util.*;
import java.io.*; 
import java.awt.*;
import java.awt.event.*;
import java.sql.Timestamp;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class EchoServer3 extends JFrame implements ActionListener{

	ArrayList<ClientUser> clients;
	ArrayList<ClientUser> gamers;
	HashMap<String, String> map; // holds UserName and UserIP
	// GUI items
	JButton createGame;
	JButton ssButton;
	JLabel machineInfo;
	JLabel portInfo;
	JTextArea history;
	JTextArea usersOnline;

	boolean running;
	private long startTime;
	P10Game p10Game;



	// Network Items
	boolean serverContinue;
	ServerSocket serverSocket;

	// set up GUI
	public EchoServer3()
	{
		super("Echo Server");

		//border is created
		JPanel borderHolder = new JPanel();
		Border raisedbevel = BorderFactory.createRaisedBevelBorder();
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		Border compound = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
		compound = BorderFactory.createTitledBorder(compound, "Other Users Online", TitledBorder.CENTER, TitledBorder.ABOVE_TOP);
		borderHolder.setBorder(compound);
		borderHolder.setPreferredSize(new Dimension(140,200));
		// get content pane and set its layout
		Container container = getContentPane();
		container.setLayout( new FlowLayout() );
		clients = new ArrayList<ClientUser>();
		gamers = new ArrayList<ClientUser>();
		map = new HashMap();

		// create buttons
		running = false;
		ssButton = new JButton( "Start Listening" );
		ssButton.addActionListener( this );
		container.add( ssButton );

		createGame = new JButton("Start Game");
		createGame.addActionListener(this);
		createGame.setEnabled(false);
		container.add(createGame);

		String machineAddress = null;
		try
		{  
			InetAddress addr = InetAddress.getLocalHost();
			machineAddress = addr.getHostAddress();
			System.out.println(machineAddress);
		}
		catch (UnknownHostException e)
		{
			machineAddress = "192.168.1.86";
		}
		machineInfo = new JLabel (machineAddress);
		container.add( machineInfo );
		portInfo = new JLabel (" Not Listening ");
		container.add( portInfo );

		history = new JTextArea ( 10, 40 );
		history.setEditable(false);
		container.add( new JScrollPane(history) );

		usersOnline = new JTextArea(10, 10);
		usersOnline.setEditable(false);
		borderHolder.add(new JScrollPane(usersOnline));

		container.add(borderHolder);

		setSize(650, 275);
		setVisible( true );

	} // end CountDown constructor

	public static void main( String args[] )
	{ 
		EchoServer3 application = new EchoServer3();
		application.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}

	// handle button event
	public void actionPerformed( ActionEvent event )
	{
		if (running == false)
		{
			running = true;
			new ConnectionThread (this);
		}




		else if (event.getSource() == createGame){
			p10Game = new P10Game(clients);
			p10Game.start();
			int i,j;

			for(i=0;i<gamers.size();i++){

				PrintWriter temp = gamers.get(i).getOut(); // creating an output stream to send inforamtion for each client.
				StringBuilder output = new StringBuilder();
				for(j=0;j<gamers.size();j++){             
					output.append(gamers.get(j).name);  // creating a string with all the usernames
					output.append(" ");
				}
				temp.println("StartGame "+output);  // sending a string with all the usernames to the clients. I am catching it on line 
				System.out.println("StartGame "+output); // 312 in EchoClient3
			}
		}
		else
		{
			serverContinue = false;
			ssButton.setText ("Start Listening");
			portInfo.setText (" Not Listening ");
			running = false;
		}
	}

} // end class EchoServer3


class ConnectionThread extends Thread
{
	EchoServer3 gui;


	public ConnectionThread (EchoServer3 es3)
	{
		gui = es3;
		start();
	}

	public void run()
	{
		gui.serverContinue = true;

		try 
		{ 
			gui.serverSocket = new ServerSocket(56424);   //port number hard coded
			gui.portInfo.setText("Listening on Port: " + gui.serverSocket.getLocalPort());
			System.out.println ("Connection Socket Created");
			new GameThread(gui.gamers, gui);

			try { 
				while (gui.serverContinue)
				{
					System.out.println ("Waiting for Connection");
					gui.ssButton.setText("Stop Listening");

					new CommunicationThread (gui.serverSocket.accept(), gui); 
				}
			} 
			catch (IOException e) 
			{ 
				System.err.println("Accept failed."); 
				System.exit(1); 
			} 
		} 
		catch (IOException e) 
		{ 
			System.err.println("Could not listen on port: 10008."); 
			System.exit(1); 
		} 
		finally
		{
			try {
				gui.serverSocket.close(); 
			}
			catch (IOException e)
			{ 
				System.err.println("Could not close port: 10008."); 
				System.exit(1); 
			} 
		}
	}
}

class CommunicationThread extends Thread
{ 
	private boolean serverContinue = true;
	private Socket clientSocket;
	private EchoServer3 gui;
	private String name;
	private boolean connected;


	public CommunicationThread (Socket clientSoc, EchoServer3 ec3)
	{
		clientSocket = clientSoc;
		gui = ec3;
		start();
	}

	public void run()
	{

		System.out.println ("New Communication Thread Started");

		try { 
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),true); 
			BufferedReader in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream())); 
			connected = true;
			String inputLine; 

			while (connected) 
			{ 
				inputLine = in.readLine();

				//gui.history.insert (inputLine+"\n", 0);
				actionTaken(inputLine, gui.clients, out);
			} 

			out.close(); 
			in.close(); 
			clientSocket.close(); 
		} 
		catch (IOException e) 
		{ 
			System.err.println("Problem with Communication Server");
			System.exit(1); 
		} 
	}


	public void actionTaken(String input, ArrayList<ClientUser> clients, PrintWriter out){


		//username and IP are put into the ArrayList of clients
		if (input.startsWith("USERNAME")){
			name = input.substring(9);
			boolean badSN = false;
			InetAddress addr = clientSocket.getInetAddress();
			String IP = addr.getHostAddress();
			for(int i = 0; i<gui.clients.size();i++){
				if(gui.clients.get(i).name.equals(name)){
					badSN = true;
					out.println("NEWSN");
					break;
				}
			}
			if(name.contains(" ")){
				badSN = true;
				out.println("NEWSN");
			}
			if(!badSN){
				gui.map.put(name, IP);
				gui.usersOnline.insert(name+"\n", 0);
				ClientUser newclient = new ClientUser(name, clientSocket, out);
				gui.clients.add(newclient);
				sendMessageToAll(name+" has joined the chatroom.");
				if(clients.size()>1)
					sendClientList(newclient, gui.clients);
			}
		}

		//Client wants to send a a normal message to all people online (MSG "message")
		else if (input.startsWith("MSG")){
			String msg = input.substring(4);
			gui.history.insert(name+": "+ msg+"\n", 0);
			sendMessageToAll(name + ": " + msg);

		}

		else if(input.startsWith("DISCONNECT")){
			connected = false;
			name = input.substring(11);
			gui.usersOnline.setText(gui.usersOnline.getText().replace(name + "\n", ""));
			
			System.out.println("Users after disconnect: \n");
			for(int i = 0;i<gui.clients.size();i++)
				System.out.println(gui.clients.get(i).name);
			sendMessageToAll(name+" has left the chatroom.");
			for(int i = 0;i<gui.clients.size();i++)
				if(gui.clients.get(i).name.equals(name)){
					System.out.println("Client removed.");
					gui.clients.get(i).setActive();
					gui.clients.remove(i);					
				}
		}


		//Client wants to send a private message to a specified user (PRIVATEMSG to "message")
		else if (input.startsWith("PRIVATEMSG")){
			int i;
			String info=input.substring(11);
			String[] parts=info.split(";");
			String msg=parts[0];
			ArrayList<String> recipients =new ArrayList<String>();
			for(i=1;i<parts.length;i++){
				recipients.add(parts[i]);
			}
			gui.history.insert(name+ ": "+msg +"\n",0);
			sendMessageToSomeUsers(name +": "+msg,recipients, out);
		}

		//User is ready and is put into the gamers List
		else if(input.startsWith("READY")){
			gui.history.insert(input + "\n", 0);
			String s = input.substring(6);
			ClientUser temp = getUserByName(s, gui.clients);
			gui.gamers.add(temp);
			System.out.println("Number of gamers ready: "+gui.gamers.size());
		}

		//user is no longer ready and is removed from the gamers list
		else if (input.startsWith("NOTREADY")){
			gui.history.insert(input +"\n", 0);
			String s = input.substring(9);
			System.out.println("Number of gamers ready: "+gui.gamers.size());
			removeGamerByName(s, gui.gamers);

		}
        else if(input.startsWith("SendingHand")){
            String[]cards1=new String[3];
            cards1=input.split(";");
            System.out.println("Inside server listener UserName:"+cards1[1]+"hand"+cards1[2]);
            int length1 = gui.clients.size();
            for (int i = 0; i < length1; i++){
                PrintWriter temp = gui.gamers.get(i).getOut();
                temp.println("UpdateHands"+";"+cards1[1]+";"+cards1[2]);
            }
        }
            else if (input.startsWith("NEWCARDDECK")){
                String n = input.substring(12).trim();
        	for(int i = 0; i < gui.gamers.size(); i++){
        		if (gui.gamers.get(i).name.equals(n)){
        			gui.p10Game.drawFromDeck(gui.gamers.get(i));
        		}
        	}
        }
		
            else if (input.startsWith("NEWCARDDIS")){
                String n = input.substring(11).trim();
                String update = "";
                
                System.out.println("here1");
                if (gui.p10Game.discardPile.size() > 0){
                    for(int i = 0; i < gui.gamers.size(); i++){
                        if (gui.gamers.get(i).name.equals(n)){
                            update = gui.p10Game.drawFromDiscard(gui.gamers.get(i));
                        }
                    }
                    
                    System.out.println("here2");
                    if (gui.p10Game.discardPile.size()-1> 0){
                        System.out.println("here3");
                        System.out.println("Size of arrayList "+gui.p10Game.discardPile.size());
                         for (int i = 0; i < gui.gamers.size(); i++){
                             PrintWriter temp = gui.gamers.get(i).getOut();
                             temp.println("UpdateDiscard "+ update);
                         }
                    }
                    else {
                        System.out.println("here4");
                           for (int i = 0; i < gui.gamers.size(); i++){
                             PrintWriter temp = gui.gamers.get(i).getOut();
                             temp.println("UpdateDiscard "+"no");
                         }
                    }
                }
                else{
                    for (int i = 0; i < gui.gamers.size(); i++){
                     PrintWriter temp = gui.gamers.get(i).getOut();
                     temp.println("UpdateDiscard "+ "noway");
                 } 
                }
            }
            else if(input.startsWith("REMOVECARD")){
                String w=input.substring(11).trim();
                gui.p10Game.discardCard(w);
                
                for (int i = 0; i < gui.gamers.size(); i++){
                    PrintWriter temp = gui.gamers.get(i).getOut();
                    temp.println("UpdateDiscard "+ w);
                }
                gui.p10Game.turnOver();
            }

	}

	public void removeGamerByName(String s, ArrayList<ClientUser> gamers ){
		int tempSize = gui.gamers.size();
		for (int i = 0; i < tempSize; i++){
			if (s.equals(gamers.get(i).getName())){
				gamers.remove(i);
				break;
			}
		}
	}


	public ClientUser getUserByName(String s, ArrayList<ClientUser> clients){
		int size = clients.size();
		for (int i = 0; i < size; i++){
			if (clients.get(i).getName().equals(s)){
				return clients.get(i);
			}
		}	
		return null;
	}


	private void sendClientList(ClientUser newclient, ArrayList<ClientUser> clients) {
		String clientlist = "?*& ";
		int receive = 0;
		for(int i = 0; i<clients.size(); i++){
			if(clients.get(i).name.equals(newclient.name))
				receive = i;
			else
				clientlist += clients.get(i).name + " ";
		}
		PrintWriter temp = clients.get(receive).getOut();
		temp.println(clientlist);
	}

	public void sendMessageToAll(String msg){
		int length = gui.clients.size();

		for (int i = 0; i < length; i++){
			PrintWriter temp = gui.clients.get(i).getOut();
			temp.println(msg);
		}
	}

	public void sendMessageToSomeUsers(String msg,ArrayList<String> recipients, PrintWriter out){
		out.println(msg);
		int length=gui.clients.size();
		for(int i=0;i<recipients.size();i++){
			boolean x = false;
			for(int j=0;j<length;j++){
				if(recipients.get(i).trim().equals(gui.clients.get(j).name)){
					PrintWriter temp = gui.clients.get(j).getOut();
					temp.println(msg);
					System.out.println("msg");
					x = true;
				}
			}
			if(x==false){
				out.println(recipients.get(i)+" is not a current user");
			}
		}

	} 

	public static String parseName(String s){
		StringTokenizer st =  new StringTokenizer(s);
		String temp = st.nextToken();
		return temp;
	}
} 

class GameThread extends Thread{
	private EchoServer3 gui;
	private ArrayList<ClientUser> users;
	private int size;


	public GameThread(ArrayList<ClientUser> list, EchoServer3 e){
		users = list;
		gui = e;

		start();
	}

	public void run(){

		while (gui.running){
			size = users.size();
			if (size >= 2 && !(gui.createGame.isEnabled())){
				gui.createGame.setEnabled(true);
			}
			else if ((size < 2 && gui.createGame.isEnabled()) || (size > 6 && gui.createGame.isEnabled()) ){
				gui.createGame.setEnabled(false);
			}


		}
	}
}





