//package src;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;


public class ClientUser {
	int ID;
	String name;
	Socket clientSocket;
	PrintWriter out;
	private ArrayList<P10Card> P10Hand;
	private int P10Phase;
	private int P10Score;
	private boolean phaseComplete;
	private boolean isSkipped;
	private boolean isActive;

	public ClientUser(String n, Socket SOCK, PrintWriter o){
		name = n;
		clientSocket = SOCK;
		out = o;
		P10Hand = new ArrayList<P10Card>();
		P10Phase = 1;
		P10Score = 0;
		phaseComplete = false;
		isSkipped = false;
		isActive=true;
	}

	public boolean getActive(){
		return isActive;
	}
	
	public void setActive(){
		isActive=false;
	}
	public PrintWriter getOut(){
		return out;
	}

	public String getName(){
		return name;
	}

	public ArrayList<P10Card> getP10Hand(){
		return this.P10Hand;
	}

	public int getP10Score(){
		return this.P10Score;
	}

	public boolean isPhaseComplete(){
		return this.phaseComplete;
	}

	public int getP10Phase(){
		return this.P10Score;
	}

	public void p10DrawCard(P10Card newCard) {
		this.P10Hand.add(newCard);
	}

	public void p10CompletePhase(){
		this.P10Phase++;
	}

	public boolean isSkipped(){
		return this.isSkipped;
	}

	public void turnSkipped(){
		this.isSkipped = !this.isSkipped;
	}




	public void p10TallyPoints(){
		ArrayList<P10Card> hand = this.P10Hand;
		for(int i = 0; i<hand.size();i++){
			if(hand.get(i).getRank()>='1' && hand.get(i).getRank()<='9')
				this.P10Score+=5;
			if(hand.get(i).getRank()=='T' || hand.get(i).getRank()<='E' || hand.get(i).getRank()<='E')
				this.P10Score+=10;
			if(hand.get(i).getRank()=='S')
				this.P10Score+=15;
			if(hand.get(i).getRank()=='W')
				this.P10Score+=25;
		}
	}

	public ArrayList<P10Card> clearHand(){
		ArrayList<P10Card> temp = this.P10Hand;
		this.P10Hand = new ArrayList<P10Card>();
		return temp;
	}

	public void removeCard(int index){
		this.P10Hand.remove(index);
	}

}
