
import java.net.*; 
import java.util.ArrayList;
import java.io.*; 
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class EchoClient3 extends JFrame implements ActionListener
{  
	private static final long serialVersionUID = 1L;
	// GUI items
	JButton sendButton;
	JButton connectButton;
	JButton readyUp;
	JTextField machineInfo;
	JTextField portInfo;
	JTextField message;
	JTextField sendTo;
	JTextArea history;
	JTextField recipient;
	String str;
	String UserIP;
	String serverIp;
	JCheckBox box;
	String[] users= new String[10];
	JPanel upperPanel = new JPanel ();
	JScrollPane usersOnlinePane;
	JTextArea usersOnline;

	//Game Items
	ArrayList<P10Card> userHand;
	JTextArea cardDisplay;
	JPanel gamePanel;
	JPanel backgroundPanel;
	JTextArea hand;
	JLabel discardPile;
	JButton discardButton;
	JButton drawDiscard;
	JButton drawCard;

	JTextField enterDiscard;



	// Network Items
	boolean ready;
	boolean privateMsg;
	boolean connected;
	Socket echoSocket;
	PrintWriter out;
	BufferedReader in;
	String UserName;
	clientThread thread;
	Container container;

	// set up GUI
	public EchoClient3()
	{

		super("Client");

		UserName = JOptionPane.showInputDialog(EchoClient3.this, "UserName: ");
		UserName.trim();

		//user must enter a valid name
		while (UserName.equals("")){
			UserName = JOptionPane.showInputDialog(EchoClient3.this, "UserName: ");
			UserName.trim();
		}

		this.setTitle(UserName + "'s ChatBox");

		privateMsg = false;




		// get content pane and set its layout
		container = getContentPane();
		container.setLayout (new BorderLayout ());
		container.add(upperPanel, BorderLayout.NORTH);
		//// set up the North panel
		//JPanel upperPanel = new JPanel ();

		upperPanel.setLayout (new GridLayout (6,2));
		;


		// create buttons
		connected = false;
		upperPanel.add ( new JLabel ("Message: ", JLabel.RIGHT) );
		message = new JTextField ("");
		message.addActionListener( this );
		upperPanel.add( message );

		sendButton = new JButton( "Send Message" );
		sendButton.addActionListener( this );
		sendButton.setEnabled (false);
		upperPanel.add( sendButton );

		connectButton = new JButton( "Connect to Server" );
		connectButton.addActionListener( this );
		upperPanel.add( connectButton );

		upperPanel.add ( new JLabel ("Server Address: ", JLabel.RIGHT) );
		try{
			InetAddress addr = InetAddress.getLocalHost();
			UserIP = addr.getHostAddress();
			machineInfo = new JTextField (UserIP);
		}
		catch(UnknownHostException e){
			UserIP ="192.168.1.86";
		}
		upperPanel.add( machineInfo);

		upperPanel.add ( new JLabel ("Server Port: ", JLabel.RIGHT) );
		portInfo = new JTextField ("56424");
		upperPanel.add( portInfo );

		upperPanel.add(new JLabel("enter intended recepients with a ';' in between",JLabel.RIGHT));
		recipient= new JTextField("");
		upperPanel.add(recipient);

		box = new JCheckBox("Public message");
		box.setSelected(true);
		box.addActionListener(this);
		upperPanel.add(box);

		ready = false;
		readyUp = new JButton("Ready Up");
		readyUp.addActionListener(this);
		readyUp.setEnabled(true);
		readyUp.setOpaque(false);
		readyUp.setForeground(Color.RED);
		upperPanel.add(readyUp);

		history = new JTextArea ( 10, 33 );
		history.setEditable(false);
		container.add(new JScrollPane(history) ,  BorderLayout.WEST);

		usersOnline = new JTextArea ( 10, 10 );
		usersOnline.setEditable(false);
		container.add(new JScrollPane(usersOnline) ,  BorderLayout.EAST);

		setSize( 600, 300 );
		setVisible( true );

	} // end CountDown constructor

	public static void main( String args[] )
	{ 
		EchoClient3 application = new EchoClient3();
		application.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}

	// handle button event
	public void actionPerformed( ActionEvent event )
	{
		if ( connected && (event.getSource() == sendButton || event.getSource() == message ) )
		{
			if(box.isSelected()&& recipient.getText().isEmpty()) // message sent to everyone
				out.println("MSG "+message.getText());
			else if(!box.isSelected() &&!recipient.getText().isEmpty()){
				System.out.println(recipient.getText());
				out.println("PRIVATEMSG"+";"+message.getText()+";"+ recipient.getText());
				message.setText("");
			}

			else if(box.isSelected()&& !recipient.getText().isEmpty()){
				JOptionPane.showMessageDialog(EchoClient3.this,
						"The box is checked and a specific user is specified!.",
						"error",
						JOptionPane.ERROR_MESSAGE);
			}

			else{
				JOptionPane.showMessageDialog(EchoClient3.this,
						"Please check the box to send the message to all users or specify the users with !.",
						"wich you want to share the message",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		else if (event.getSource() == connectButton)
		{
			if(connected){
				System.out.println("DISCONNECT " + UserName);
				out.println("DISCONNECT " + UserName);
				doManageConnection();
				usersOnline.setText("");
				history.insert("You have left the chatroom.\n", 0);
			}
			else if(!connected){
				doManageConnection();
				out.println("USERNAME " + UserName);
			}
		}
		//lets the server know this user is ready
		else if (connected && event.getSource() == readyUp){
			if(!ready){
				out.println("READY "+ UserName);
				readyUp.setText("Ready");
				readyUp.setForeground(Color.GREEN);
				readyUp.setEnabled(false);
				ready = true;
			}
			//else if()
			else{
				out.println("NOTREADY " + UserName);
				readyUp.setText("Ready Up");
				readyUp.setForeground(Color.RED);
				ready = false;
			}
		}

	}

	public boolean checkPhase(ArrayList<P10Card> hand, int phase){
		String handString = new String("");
		ArrayList<String> cardInput = new ArrayList<String>();
		for(int i =0; i<hand.size(); i++)
			handString += "" + (i+1) + ". " + hand.get(i).getString() + "\n";
		if(phase == 1){
            cardInput.add(0,JOptionPane.showInputDialog(this,"Current Phase: 1 -- 2 Sets of 3\nPlease enter your cards (Ex. S1,S1,S1,S2,S2,S2):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            int pointVal = 0;
            for(int i = 0; i<cardList.size();i++){
                if(cardList.get(i).getRank()=='W')
                    pointVal+=2;
            }
            if(cardList.get(0).getRank() == cardList.get(2).getRank())
                pointVal++;
            if(cardList.get(3).getRank() == cardList.get(5).getRank())
                pointVal++;
            if(cardList.get(0).getRank() == cardList.get(1).getRank())
                pointVal++;
            if(cardList.get(2).getRank() == cardList.get(3).getRank())
                pointVal++;
            if(cardList.get(4).getRank() == cardList.get(5).getRank())
                pointVal++;
            if(cardList.get(3).getRank() == cardList.get(4).getRank())
                pointVal++;
            if(pointVal==6)
                return true;
            return false;
        }
        else if(phase == 2){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 2 -- 1 Set of 3 + 1 Run of 4\nPlease select your cards (Ex. S1,S1,S1,R1,R1,R1,R1):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (setValidation(cardList,0,3)&&runValidation(cardList,3,7));
        }
        else if(phase == 3){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 3 -- 1 Set of 4 + 1 Run of 4\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (setValidation(cardList,0,4)&&runValidation(cardList,4,8));
        }
        else if(phase == 4){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 4 -- 1 Run of 7\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (runValidation(cardList,0,7));
        }
        else if(phase == 5){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 5 -- 1 Run of 8\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (runValidation(cardList,0,8));
        }
        else if(phase == 6){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 6 -- 1 Run of 9\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (runValidation(cardList,0,9));
        }
        else if(phase == 7){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 7 -- 2 Sets of 4\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (setValidation(cardList,0,4)&&setValidation(cardList,4,8));
        }
        else if(phase == 8){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 8 -- 7 Cards of 1 Color\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            char c=cardList.get(0).getColor();
            for(int i=0;i<cardList.size();i++){
            	if(cardList.get(i).getColor()!=c){
            		if(cardList.get(i).rankNo()!=69){
            			return false;
            		}
            	}
            }
            return true;
        }
        else if(phase == 9){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 9 -- 1 Set of 5 + 1 Run of 2\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (setValidation(cardList,0,5)&&setValidation(cardList,5,7));
        }
        else if(phase == 10){
            cardInput.add(JOptionPane.showInputDialog(this,"Current Phase: 10 -- 1 Set of 5 + 1 Run of 3\nPlease select your cards (separated by a ','):\n\n" + handString));
            ArrayList<P10Card> cardList = new ArrayList<P10Card>();
            String[] cardsPicked = cardInput.get(0).split(",");
            int cardInt = -1;
            int index = 0;
            for(int i = 0; i<cardsPicked.length;i++){
            
                try {
                    cardInt = Integer.parseInt(cardsPicked[i]);
                    cardList.add(hand.get(cardInt));
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                    return false;
                }
                 if((cardInt<1 || cardInt>hand.size()) && cardInt!=87){
                     JOptionPane.showMessageDialog(this, "The input entered did not result in a complete phase!\nPlease try again.\n");
                     return false;
                 }
            }
            return (setValidation(cardList,0,5)&&setValidation(cardList,5,8));
        }
        return false;
    }
    
    public boolean setValidation(ArrayList<P10Card> cardList, int lowerbound,int upperbound){
    	for(int i=lowerbound;i<upperbound;i++){
        	for(int j=lowerbound;j<upperbound;j++){
        		if(cardList.get(i).rankNo()!=cardList.get(j).rankNo()){
        			if((cardList.get(i).rankNo())!=69&&(cardList.get(i).rankNo()!=69)){
        				return false;
        			}
        		}
        	}
        }
    	return true;
    }
    public boolean runValidation(ArrayList<P10Card> cardList, int lowerbound,int upperbound){
    	boolean flag=false;int prev=0;
    	for(int i=lowerbound;i<upperbound;i++){
    		if(cardList.get(i).rankNo()!=69){
    			if(!flag){flag=true;prev=cardList.get(i).rankNo();}	
    			else if((cardList.get(i).rankNo()-1)!=prev){
    				return false;
    				}
    			prev++;
    			}
    		else{
    			if(flag){
    				prev++;
    			}
    		}
    	}
    	return true;
    }
	public void doManageConnection()
	{
		if (connected == false)
		{
			String machineName = null;
			int portNum = -1;
			String intended_user=null;
			try {

				machineName = machineInfo.getText();
				portNum = Integer.parseInt(portInfo.getText());
				intended_user= recipient.getText();
				String [] recipients = intended_user.split(" "); // creating an array with private recipients
				echoSocket = new Socket(machineInfo.getText(), portNum );
				out = new PrintWriter(echoSocket.getOutputStream(), true);
				in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
				sendButton.setEnabled(true);
				connected = true;
				connectButton.setText("Disconnect from Server");
				thread = new clientThread(this);

			} catch (NumberFormatException e) {
				history.insert ( "Server Port must be an integer\n", 0);
			} catch (UnknownHostException e) {
				history.insert("Don't know about host: " + machineName + "\n", 0);
			} catch (IOException e) {
				history.insert ("Couldn't get I/O for "
						+ "the connection to: " + machineName +"\n", 0);
			}

		}
		else
		{
			try 
			{
				connected = false;
				out.close();
				in.close();
				echoSocket.close();
				sendButton.setEnabled(false);

				connectButton.setText("Connect to Server");
			}
			catch (IOException e) 
			{
				history.insert ("Error in closing down Socket ", 0);
			}
		}


	}

	public class clientThread extends Thread implements ActionListener{
		private EchoClient3 client;
		private EchoServer3 server;
		ArrayList<ClientUser> gamers1=new ArrayList<ClientUser>();

		public clientThread(EchoClient3 c){
			client = c;
			start();
		}

		public void run(){

			try {

				String inputLine;
				ArrayList<JLabel>users=new ArrayList<JLabel>();

				while (connected){
					inputLine = client.in.readLine();
					if(inputLine.equals(client.UserName + " has joined the chatroom.")){
						client.history.insert("You have joined the chatroom.\n",0);
						client.usersOnline.insert(client.UserName + "\n", 0);
					}
					else if(inputLine.startsWith("It is now " + client.UserName + "'s turn.")){
						startTurn();
					}

					else if(!inputLine.split(" ")[0].contains(":") && inputLine.startsWith("NEWSN") && !client.UserName.equals("NEWSN")){
						UserName = JOptionPane.showInputDialog(EchoClient3.this, "Username already taken or contains spaces!\nPlease select a different username: ");
						client.out.println("USERNAME " + UserName);
						client.setTitle(UserName + "'s ChatBox");
					}
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.startsWith("?*&") && !client.UserName.equals("?*&")){
						String[] userlist = inputLine.split(" ");
						for(int i=1; i<userlist.length;i++)
							if(!client.usersOnline.getText().contains(userlist[i] + "\n"))
								client.usersOnline.insert(userlist[i] + "\n", 0);
					}

					else if(!inputLine.contains(":") && inputLine.startsWith("?*&?*&?*&?")){
						System.out.println(inputLine);
						String [] rec= inputLine.split(";");
						for(int i=2;i<rec.length;i++){
							client.history.insert(inputLine+".\n",0);
						}
					}

					else if(!inputLine.split(" ")[0].contains(":") && inputLine.contains(" has joined the chatroom.")){
						client.history.insert(inputLine + "\n",0);
						String[] splitSN = inputLine.split(" ");
						System.out.println("SN parsed: " + splitSN[0]);
						if(!client.usersOnline.getText().contains(splitSN[0] + "\n"))
							client.usersOnline.insert(splitSN[0] + "\n", 0);
					}
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.contains(" has left the chatroom.")){
						if(!inputLine.startsWith(UserName + " ")){
							client.history.insert(inputLine + "\n",0);
							String[] parsedSN = inputLine.split(" ");
							String removeSN = parsedSN[0];
							String[] splitUserList = client.usersOnline.getText().split("\n");
							String newUserList = "";
							for(int i = 0; i<splitUserList.length;i++){
								if(!splitUserList[i].equals(removeSN))
									newUserList += splitUserList[i] + "\n";
							}
							client.usersOnline.setText(newUserList);
						}

					}

					else if(inputLine.startsWith("UpdateHands")){
						int i;
						String[] Input=inputLine.split(";");
						for(i=0;i<gamers1.size();i++){
							if(gamers1.get(i).name.equals(Input[1])){
								users.get(i).setText(Input[2]);
							}
						}
					}
					
                    else if(inputLine.startsWith("UpdateDiscard")){
                        String s = inputLine.substring(14).trim();
                        System.out.println("hi there: "+s);
                        if (s.equals("no")){
                            System.out.println("NO");
                            discardPile.setText("");
                        }
                        else if (s.equals("noway")){
                            history.insert("You can't do that!!!!!!!!!!", 0);
                        }
                        discardPile.setText(s);
                    }



					else if(inputLine.startsWith("CARD ")){
						//userHand=new ArrayList<P10Card>();
						System.out.println("This is the card line "+inputLine);
						String substring = inputLine.substring(5);
						String[] Input = substring.split(" ");
						System.out.println("Input String :" + inputLine);
						P10Card myCard = new P10Card(Input[1].charAt(0),Input[2].charAt(0));
						System.out.println(myCard.getString());
						userHand.add(myCard);
						int i;
						StringBuilder my_card_hand= new StringBuilder();
						int index = 0;

						client.hand.insert(Character.toString(myCard.getRank())+Character.toString(myCard.getColor())+"\n", 0);
						// System.out.println("This is the size of users:"+users.get(0).getText() +users.get(1).getText());

					}

					else if (inputLine.startsWith("StartGame")){
						wipeGUI();
						makeGameGUI();
					}
					else{
						if(inputLine.length()>55){
							int numLines = inputLine.length()/55;
							String insertLines = "";
							for(int i = 0;i<numLines;i++){
								insertLines += inputLine.substring(55*i,55*(i+1)-1) + "\n";
							}
							client.history.insert(insertLines, 0);
						}
						else{
							client.history.insert(inputLine + "\n", 0);
						}
					}
				}
			} catch (IOException e) {
			}

		}

		private void startTurn() {
			client.drawCard.setEnabled(true);
			client.drawDiscard.setEnabled(true);
		}

		public void setTextColor(char Color){

		}

		public void wipeGUI(){
			client.container.remove(upperPanel);
			client.container.remove(usersOnline);
			client.container.remove(history);
		}

		public void makeGameGUI(){
			JLabel discardLabel = new JLabel("Top of Discard:");
			userHand=new ArrayList<P10Card>();
			discardPile =new JLabel();
			gamePanel = new JPanel(null);
			discardButton = new JButton("Discard");
			discardButton.setEnabled(false);
			//drawCard=new JButton("Draw");
            drawDiscard = new JButton("Draw Discard");
			//discardButton.addActionListener((ActionListener) this);
			//drawCard.addActionListener((ActionListener) this);
            drawDiscard = new JButton("Draw Discard");
            drawCard=new JButton("Draw Deck");
            discardButton.addActionListener(this);
            drawCard.addActionListener( this);
            drawDiscard.addActionListener(this);

            enterDiscard = new JTextField();
			client.backgroundPanel = new JPanel(null);
			client.cardDisplay = new JTextArea(10,33);
			client.hand = new JTextArea(10, 10);
			//client.discardPile.setBackground(Color.BLACK);
            client.discardPile.setBackground(Color.cyan);
            drawDiscard.setBounds(170, 60, 150, 40);
            discardLabel.setBounds(280, 10, 100, 50);
     		discardPile.setBounds(390, 10, 50, 50);
			discardPile.setOpaque(true);
			client.usersOnline.setBounds(785, 220, 100, 100);
			client.history.setBounds(490, 10, 400, 200);
			client.message.setBounds(490, 220, 200, 30);
			client.sendButton.setBounds(700, 220, 75, 30);
			client.enterDiscard.setBounds(170, 10, 100,40);
			client.discardButton.setBounds(10, 10, 150, 40);
			client.drawCard.setBounds(10,60,150,40);
			client.gamePanel.setBounds(10, 10, 450, 450);
			client.hand.setBounds(10,270,100 , 200);
			client.cardDisplay.setBounds(115, 270, 350, 200);
			client.sendButton.setText("Send");

			
			client.gamePanel.setBackground(Color.GREEN);
			client.gamePanel.add(discardLabel);
			client.gamePanel.add(drawDiscard);
			client.gamePanel.add(discardButton);
			client.gamePanel.add(discardPile);
			client.gamePanel.add(enterDiscard);
			client.gamePanel.add(client.cardDisplay);
			client.gamePanel.add(client.hand);
			client.gamePanel.add(client.discardButton);
			client.gamePanel.add(client.drawCard);
			client.gamePanel.add(client.drawDiscard);
			client.gamePanel.add(client.enterDiscard);


			client.backgroundPanel.add(usersOnline);
			client.backgroundPanel.add(history);
			client.backgroundPanel.add(message);
			client.backgroundPanel.add(sendButton);
			client.backgroundPanel.add(gamePanel);

			client.setSize(900,500);
			container.add(backgroundPanel);
		}

		@Override
		public void actionPerformed(ActionEvent Event) {
			if(Event.getSource()==drawCard){
                out.println("NEWCARDDECK "+client.UserName);
                drawCard.setEnabled(false);
                drawDiscard.setEnabled(false);
                discardButton.setEnabled(true);
			}
            else if (Event.getSource() == drawDiscard){
                out.println("NEWCARDDIS "+client.UserName);
                drawCard.setEnabled(false);
                drawDiscard.setEnabled(false);
            }
			else if(Event.getSource()==discardButton){
				String discardCard= enterDiscard.getText().toUpperCase().trim();
				System.out.println("discard :" + discardCard);
				enterDiscard.setText("");
				String[] myCards=new String[11];
				String Ccard;
				Ccard=hand.getText().replace("\n"," ");
				System.out.println(Ccard);
				myCards=Ccard.split(" ");

				for(int i=0;i<myCards.length;i++){

					if((Character.toString(myCards[i].charAt(0))+Character.toString(myCards[i].charAt(1))).equals(discardCard)){
                        out.println("REMOVECARD "+myCards[i]);
						discardCard(discardCard);
						break;
					}
				}

			}

		}

		public void discardCard(String discardCard){
			for (int i = 0; i < userHand.size(); i++){
	            if ((Character.toString(userHand.get(i).getRank())+Character.toString(userHand.get(i).getColor())).equals(discardCard)){
					userHand.remove(i);
					break;
				}
			}
			System.out.println("Size "+userHand.size());
			client.hand.setText("");

			for(int i = 0; i < userHand.size(); i++){
                client.hand.insert((Character.toString(userHand.get(i).getRank())+Character.toString(userHand.get(i).getColor())) + "\n", 0);
			}
		}

	}
} // end class EchoServer3
